var arrayObj=[];
(function main(){
    $(".main>input").bind('keypress', (e) => {
        var todoVal = $(`.main>input`).val();
        if (e.which == 13&&todoVal.length>0) {
            arrayObj.push(createNewListObj(todoVal));
            refreshDOM(arrayObj);
            document.querySelector(".main>input").value="";
        }
    });
})();

function refreshDOM(){
    var listOfItems=document.getElementsByClassName('item-list')[0];
    listOfItems.innerHTML="";
    console.log("listOfItems after remove");
    console.log(listOfItems);
    console.log(arrayObj);
    for(var x in arrayObj)
    {
        var item=getListEle(arrayObj[x].value,arrayObj[x].status);
        item.setAttribute('position',x);
        listOfItems.appendChild(item);
    }
    $('ul input[type="checkbox"]').change(liClick);
    $('ul li button').click(deleteItem);
}
var deleteItem=(e)=>{
    e.stopPropagation()
    console.log("in delete");
    var liEle = e.target.parentNode;
    var position = liEle.getAttribute('position');
    
    arrayObj.splice(position,1);
    console.log(arrayObj);
    refreshDOM();
}
var liClick = function (e) {
    e.stopPropagation();
    var liEle = e.target.parentNode.parentNode;
    console.log(liEle);
    var label = e.target.nextElementSibling;
    var position = liEle.getAttribute('position');
    var val_toggle = label.innerText;
    if (arrayObj[position].status==false) {
        arrayObj[position].status=true //complete
    } else {
        arrayObj[position].status=false;
    }
    refreshDOM();
}

function createNewListObj(todoVal){
    var listObj={
        value: todoVal,
        status: false
    };
    return listObj;
}

function getListEle(value, status) {
    var li = document.createElement("li");
    var cross=document.createElement("button");
    cross.innerHTML="\u274C";
    cross.style.fontSize="8px";
    cross.style.float="right";
    li.setAttribute('draggable', 'true');
    if (status == true)
        li.innerHTML = `<div><input type="checkbox" checked 
><label class="complete">${value}</label></div>`;
    else
        li.innerHTML = `<div><input type="checkbox" 
><label >${value}</label></div>`;
li.getElementsByTagName('div')[0].style.display="inline-block";
li.appendChild(cross);
    return li;
}