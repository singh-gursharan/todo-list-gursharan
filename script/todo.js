$(document).ready(function () { //equivalent to documentContentLoad
    var map = {};
    var nOfComplete = 0;
    var listStatus = "All";
    var toggleStatus = "none";
    var liClick = function (e) {
        var label = e.target.nextElementSibling;
        var val_toggle = label.innerText;
        if (map[`${val_toggle}`] == -1) {
            $(label).addClass('complete');
            map[`${val_toggle}`] = 1; //complete
            nOfComplete++;
        } else {
            $(label).removeClass('complete');
            map[`${val_toggle}`] = -1;
            nOfComplete--;
        }
        if (nOfComplete == Object.entries(map).length) {
            $('.list-section>label').css('color', 'black');
        } else {
            $('.list-section>label').css('color', 'gray');
        }
    }
    $(".main>input").bind('keypress', (e) => {
        var todoVal = $(`.main input`).val();

        if (e.which == 13&&todoVal.length>0) {
            map[`${todoVal}`] = -1;
            $(".main>input").val("");
            $(".item-list").append(`<li draggable="true"><div><input type="checkbox" 
    class="toggle"><label>${todoVal}</label></div></li>`);
            $('.list-section').css('display', 'block');
            $('.list-section>label').css('color', 'gray');

            //the below function is inside this because it need to be executed after the creation of li tag.
            $('ul input[type="checkbox"]').off('click');
            $('ul input[type="checkbox"]').click(liClick);
            // $('.item-list>li').off('dragstart');
            // $('.item-list>li').off('drop');
            // $('.item-list>li').off('dropover');
        }
    });
    var dragItem = document.querySelector('.item-list');

    dragItem.addEventListener('dragstart', (ev) => {
        let dragElement = ev.target;
        console.log(ev.target);
        var className = dragElement.getElementsByTagName('div')[0].getElementsByTagName('label')[0].className;
        console.log(`class name=${className}`);
        let data = dragElement.getElementsByTagName('div')[0].getElementsByTagName('label')[0].innerHTML;
        console.log(`class name=${data}`);
        let liCheckedStatus = dragElement.getElementsByTagName('div')[0].getElementsByTagName('input')[0].checked;
        liCheckedStatus = JSON.parse(liCheckedStatus);
        console.log(`checkedStatus=${liCheckedStatus}`);
        ev.target.style.opacity = .5;
        
        ev.dataTransfer.setData('className', className);
        ev.dataTransfer.setData('data', data);
        ev.dataTransfer.setData('liCheckedStatus', liCheckedStatus);
        console.log("offsetTop:");
        console.log(ev.target.offsetTop)
        ev.dataTransfer.setData('dragOffset',ev.target.offsetTop);
        ev.dataTransfer.effectAllowed = "move";
        console.log('drag started');
    });
    dragItem.addEventListener('dragover', (ev) => {
        //console.log("in dragover ev.target is: ");
        //console.log(ev.target);
        ev.preventDefault();
      //  console.log(ev.target.parentNode.offsetTop);
        ev.dataTransfer.dropEffect = "move";
        //console.log('dragover started');
    });
    dragItem.addEventListener('drop', (ev) => {
         console.log("in drop ev.target is: ");
        console.log(ev.target);
        console.log("dropOffset")
        console.log(ev.target.parentNode.offsetTop);
        var dropOffset=ev.target.parentNode.offsetTop;
        var dragOffset=ev.dataTransfer.getData('dragOffset');
        
        ev.preventDefault();
        ev.dataTransfer.dropEffect = "move";
        console.log("this is" + this);
        let className = ev.dataTransfer.getData('className');
        let liCheckedStatus = ev.dataTransfer.getData('liCheckedStatus');
        let data = ev.dataTransfer.getData('data');
        let addElement = getListEle(className, data, liCheckedStatus);
        var dropZoneEle = ev.target.parentNode;
        if(dropZoneEle.tagName=="DIV")
        dropZoneEle=dropZoneEle.parentNode;
        //dropZoneEle.before(data);
        if(dropOffset>dragOffset)
        dropZoneEle.parentNode.insertBefore(addElement, dropZoneEle.nextSibling);
        else
        dropZoneEle.parentNode.insertBefore(addElement, dropZoneEle);

         $('ul input[type="checkbox"]').off('click');
        $('ul input[type="checkbox"]').click(liClick);
        console.log("dropped");
    });
    dragItem.addEventListener('dragend', (ev)=>{
        console.log("at drop:------------------------");
        ev.target.remove();
        
        console.log(ev.target);
        
    });
    $('.todo-status-selector button').click((e) => {
        var btn = e.target;
        var selector = btn.innerText;
        switch (selector) {
            case "All":
                if (listStatus !== "All") {
                    $(".item-list").empty();
                    for (var key in map) {
                        if (map[key] == -1) {
                            $(".item-list").append(`<li><div><input type="checkbox" 
                    class="toggle"><label>${key}</label></div></li>`);
                        } else {
                            $(".item-list").append(`<li><div><input type="checkbox" 
                    class="toggle"><label class="complete">${key}</label></div></li>`);
                        }
                    }

                }
                listStatus = "All";
                break;
            case "Active":
                if (listStatus != "Active") {
                    if (listStatus == "All") {
                        $(".item-list .complete").parent().parent().remove();
                    } else {
                        $(".item-list").empty();
                        for (var key in map) {
                            if (map[key] == -1) {
                                $(".item-list").append(`<li><div><input type="checkbox" 
                            class="toggle"><label>${key}</label></div></li>`);
                            }

                        }

                    }
                }
                listStatus = "Active";
                break;
            case "Completed":
                if (listStatus != "Complete") {
                    if (listStatus == "All") {
                        $(".item-list label:not(.complete)").parent().parent().remove();
                    } else {
                        $(".item-list").empty();
                        console.log(map);
                        for (var key in map) {
                            if (map[key] == 1) {
                                $(".item-list").append(`<li><div><input type="checkbox" 
                            class="toggle"><label class="complete">${key}</label></div></li>`);
                            }
                        }
                    }
                }
                listStatus = "Complete";
        }
        $('ul input[type="checkbox"]').off('click');
        $('ul input[type="checkbox"]').click(liClick);
    })
    $('.list-section>label').click((e) => {
        if (Object.entries(map).length != nOfComplete) {
            $('.item-list>li>div>label').addClass('complete');
            $('ul input[type="checkbox"]').prop('checked', true);
            for (var key in map) {
                if (map[key] == -1) {
                    map[key] = 1;
                    nOfComplete++;
                }
            }
            $('.list-section>label').css('color', 'black');
        } else {
            for (var key in map) {
                $('.item-list>li>div>label').removeClass('complete');
                $('ul input[type="checkbox"]').prop('checked', false);
                if (map[key] == 1) {
                    map[key] = -1;
                    nOfComplete--;
                }
            }
            $('.list-section>label').css('color', 'gray');
        }

    })
    if (Object.entries(map).length === 0) {
        $('.list-section').css('display', 'none');
    }

    function getListEle(className, data, liCheckedStatus) {
        var li = document.createElement("li");
        li.setAttribute('draggable', 'true');
        if (liCheckedStatus == "true")
            li.innerHTML = `<div><input type="checkbox" checked 
    ><label class="${className}">${data}</label></div>`;
        else
            li.innerHTML = `<div><input type="checkbox" 
    ><label class="${className}">${data}</label></div>`;
           
        return li;
    }

});
